<?php
/**
 *  個別ページ
 */
get_header();
?>


<div class="breadcrumb-wrap">
  <div class="container">
    <?php the_breadcrumb();?>
  </div>
</div>


<div id="main-visual" class=""></div>


<main id="main-lauout" class="">

  <div class="archive-list">

      <?php if ( have_posts() ) :

        while ( have_posts() ) : the_post(); ?>

         <article>

             <section>
                <div class="container">
                  <header>
                     <h1 class="h1 text-center"><?php the_title() ?></h1>
                  </header>

                  <?php the_content(); ?>


                </div>
             </section>

         </article>

        <?php endwhile; ?>

 <?php endif; ?>

    </div>


</main>



<?php get_footer(); ?>

