(function () {
  jQuery(document).ready(function ($) {
    var wsw;
    var breakpoint = 1100;

    $(window)
      .on("resize orientationchange", function (e) {
        wsw = window.screen.width;
      })
      .trigger("resize");

    function changeHeader() {
      // ヘッダー部
      if ($(window).scrollTop() >= 50) {
        $("#header").addClass("on");
      } else if ($("#header").hasClass("on")) {
        $("#header").removeClass("on");
      }
      // フッターページトップ
      if ($(window).scrollTop() >= 100) {
        $("#return_top").fadeIn("fast");
      } else {
        $("#return_top").fadeOut("fast");
      }
    }

    /* ヘッダースクロールに応じた背景色の変更 */
    window.addEventListener("scroll", function (e) {
      changeHeader();
    });

    //初期化
    changeHeader();

    // #アンカーをクリックした場合の処理
    $('a[href^="#"],#return_top a').on("click", function () {
      var speed = 400; // ミリ秒
      var href = $(this).attr("href");
      var target = $(href == "#" || href == "" ? "html" : href);
      if (!target) return false;
      var position = target.offset().top;
      position -= $("#header").outerHeight();
      $("body,html").animate({ scrollTop: position }, speed, "swing");
      return false;
    });

    // #ハッシュ値が存在する場合はハッシュ要素までスクールする
    var urlHash = location.hash;
    if (urlHash.length) {
      var speed = 200; // ミリ秒
      var target = $(urlHash);
      if (!target) return false;
      var position = target.offset().top;
      position -= $("#header").outerHeight();
      $("body,html").animate({ scrollTop: position }, speed, "swing");
    }
  });
})();
