<?php  
$tel_number = '03-6263-8163';
?>
<div class="contact-info shadow">
  <header>
    <h2 class="text-center key-color">お問い合わせ</h2>
  </header>
  <p class="read text-center">
    資料のご請求やご質問等がございましたら、 <br />
    お電話またはWebフォームよりお問い合わせ下さい。
  </p>

  <div class="flex-h start">
    <dl class="column">
      <dt class="h2">お電話でのお問い合わせ</dt>
      <dd>
        <p>ご質問等がございましたら、お気軽にご連絡ください。</p>
        <a class="btn tel" href="tel:<?= $tel_number ?>">
          <img
            class="auto"
            src="<?= get_template_directory_uri() ?>/img/h_tel.png"
            alt=""
          />
          <div class="flex-column">
            <div class="primary"><?= $tel_number ?></div>
            <div>電話受付10:00～17:00</div>
          </div>
        </a>
      </dd>
    </dl>
    <dl class="column">
      <dt class="h2">Webフォームでのお問い合わせ</dt>
      <dd>
        <p></p>
        <a class="btn mail" href="mail:">
          <img
            class="auto"
            src="<?= get_template_directory_uri() ?>/img/h_mail.png"
            alt=""
          />
          <div class="flex-column">
            <div>24時間受付中</div>
            <div class="primary">WEBご予約</div>
          </div>
        </a>
      </dd>
    </dl>
  </div>
</div>
