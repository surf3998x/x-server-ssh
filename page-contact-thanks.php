<?php
/**
 *  Template Name: お問い合わせ・送信完了
 */
get_header();
?>


<div class="breadcrumb-wrap">
  <div class="container">
    <?php the_breadcrumb();?>
  </div>
</div>


<div id="main-visual" class="contact"></div>


<main id="main-lauout" class="contact">

     <article>

         <section>
            <div class="container">
              <header>
                 <h1 class="h1 text-center">送信完了</h1>
              </header>

              <div class="read-box">
                <p class="read">
                 お問合せ頂きまして、誠にありがとうございます。 <br>
                 送信は正常に完了しました。
                 </p>

                 <ul class="kome">
                   <li><p>E-Mail ・ フォームによるお問い合わせの場合、回答までに最大で土日 ・ 祝日を除いた2営業日程度を頂いております。</p></li>
                   <li><p>回答が届かない場合、【   info@zeevaa.com  】までご連絡をお願いいたします。 </p></li>
                 </ul>
               </div>

            </div>
         </section>

     </article>


</main>



<?php get_footer(); ?>

