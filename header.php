<?php
$tel_number = '03-6263-8163';
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="ie lt-ie9 lt-ie8" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="ie lt-ie9" lang="ja"> <![endif]-->
<!--[if lte IE 9]> <html class="ie IE" lang="ja"> <![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="icon" type="image/png" href="<?= get_template_directory_uri() ?>/img/favicon.png">
  <link rel="stylesheet" href="<?= get_stylesheet_uri() ?>"><!-- 初期設定CSS -->
  <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/css/commons.css"><!-- 共通ヘッダー、フッター -->
  <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/css/contact-info.css"><!-- 共通コンタクト -->
  <link rel="stylesheet" href="<?= getPageCSSFilePath() ?>"><!-- ページコンテンツCSS -->
	<?php wp_head(); ?>

<!--[if lt IE 9]>
<script src="<?= get_template_directory_uri() ?>/js/html5shiv.js"></script>
<script src="<?= get_template_directory_uri() ?>/js/respond.min.js"></script>
<![endif]-->
</head>
<body <?php body_class(); ?>>


<!-- <div class="container" style="position: relative;">
<div id="left"></div>
<div id="center"></div>
<div id="right"></div>
</div> -->


<header id="header">
    <div class="container">
        <div class="h_nav">
            <a class="site-logo" href="<?= home_url(); ?>">
              <small>がん免疫療法の専門医療機関</small>
              <img src="<?= get_template_directory_uri() ?>/img/site_logo.svg" alt="サイトロゴ">
            </a>
            <div class="site-info">
              <ul class="flex-h info">
                <li class="arrow"><a class="gray" href="#">よくある質問</a></li>
                <li class="gray">【 診療時間 】月～土曜日 10:00 ～ 17:00　【 休診 】日曜日・祝日</li>
              </ul>
              <div class="header-info">
                <a class="btn tel" href="tel:<?= $tel_number ?>">
                  <img src="<?= get_template_directory_uri() ?>/img/h_tel.png" alt="">
                  <div class="txt">
                    <div class="primary"><?= $tel_number ?></div>
                    <div>電話受付10:00～17:00</div>
                  </div>
                </a>
                <a class="btn mail" href="mail:">
                  <img src="<?= get_template_directory_uri() ?>/img/h_mail.png" alt="">
                  <div class="txt">
                    <div>24時間受付中</div>
                    <div class="primary">WEBご予約</div>
                  </div>
                </a>
              </div>

            </div>
        </div>
         <!-- PC/タブレット ナビゲーション-->
         <ul class="nav pc">
            <li class="parent">
              <a class="menu" href="<?= home_url('about') ?>">
              <img class="auto" src="<?= get_template_directory_uri() ?>/img/nav_icon1.png" alt="">クリニック紹介</a>
            </li>
            <li class="parent">
              <a class="menu" href="<?= home_url('cancer') ?>">
                <img class="auto" src="<?= get_template_directory_uri() ?>/img/nav_icon2.png" alt="">ガン治療
              </a>
            </li>
            <li class="parent">
              <a class="menu" href="<?= home_url('treatment') ?>">
                <img class="auto" src="<?= get_template_directory_uri() ?>/img/nav_icon3.png" alt="">当院のがん治療
              </a>
                <ul class="sub-menu pc-only">
                  <li class="menu-item">
                    <a href="<?= home_url('flow') ?>">治療の流れ</a>
                  </li>
                  <li class="menu-item">
                    <a href="<?= home_url('case') ?>">症例・治験状況</a>
                  </li>
                  <li class="menu-item">
                    <a href="<?= home_url('about') ?>#room">院内施設のご紹介</a>
                  </li>
                </ul>
            </li>
            <li class="parent">
              <a class="menu" href="<?= home_url('access') ?>">
                <img class="auto" src="<?= get_template_directory_uri() ?>/img/nav_icon4.png" alt="">交通アクセス
              </a>
            </li>
          </ul>
    </div>
    


<!-- スマホ・ナビゲーション-->
  <div class="h_menu">
       <div class="container">
          <nav id="gNav">
             <ul class="nav">
              <li class="parent"><a href="<?= home_url() ?>">トップページ</a>
              <li class="parent"><a href="<?= home_url('about') ?>">クリック紹介</a>
                  <ul class="sub-menu acc-child-close">
                    <li class="menu-item">
                      <a href="<?= home_url('about') ?>#message">ご挨拶</a>
                    </li>
                    <li class="menu-item">
                      <a href="<?= home_url('about') ?>#introducing">院長紹介</a>
                    </li>
                    <li class="menu-item">
                      <a href="<?= home_url('about') ?>#room">院内施設のご紹介</a>
                    </li>
                  </ul>
              </li>
              <li class="parent"><a href="<?= home_url('cancer') ?>">がん治療について</a>
                 <ul class="sub-menu acc-child-close">
                    <li class="menu-item">
                      <a href="<?= home_url('cancer') ?>#problem">がん標準治療の問題点</a>
                    </li>
                    <li class="menu-item">
                      <a href="<?= home_url('cancer') ?>#structure">がん細胞の増える仕組みと解決策</a>
                    </li>
                    <li class="menu-item">
                      <a href="<?= home_url('cancer') ?>#solution">がん免疫活性化の鍵は樹上細胞にあり</a>
                    </li>
                    <li class="menu-item">
                      <a href="<?= home_url('cancer') ?>#necessary">免疫力の強化が必要不可欠</a>
                    </li>
                  </ul>
              </li>
              <li class="parent"><a href="<?= home_url('treatment') ?>">当院のがん治療</a>
                  <ul class="sub-menu acc-child-close">
                      <li class="menu-item">
                        <a href="<?= home_url('treatment') ?>#vaccine">WT1樹状細胞ワクチン療法とは</a>
                      </li>
                      <li class="menu-item">
                        <a href="<?= home_url('treatment') ?>#lyphocyte">活性化リンパ球療法</a>
                      </li>
                      <li class="menu-item">
                        <a href="<?= home_url('treatment') ?>#inhibitor">免疫チェックポイント阻害剤</a>
                      </li>
                  </ul>
              </li>
              <li class="parent"><a href="<?= home_url('flow') ?>">治療の流れ</a>
                  <ul class="sub-menu acc-child-close">
                    <li class="menu-item">
                      <a href="<?= home_url('flow') ?>#flow">治療の流れ</a>
                    </li>
                    <li class="menu-item">
                      <a href="<?= home_url('flow') ?>#document">診療に必要な書類・資料</a>
                    </li>
                  </ul>
              </li>
              <li class="parent"><a href="<?= home_url('case') ?>">症例・治験状況</a>
                      <ul class="sub-menu acc-child-close">
                        <li class="menu-item">
                          <a href="<?= home_url('case') ?>#case1">症例１</a>
                        </li>
                        <li class="menu-item">
                          <a href="<?= home_url('case') ?>#case2">症例２</a>
                        </li>
                        <li class="menu-item">
                          <a href="<?= home_url('case') ?>#case">症例実績</a>
                        </li>
                        <li class="menu-item">
                          <a href="<?= home_url('case') ?>#clinical_trial">治験状況</a>
                        </li>
                      </ul>
                  </li>               
                  <li class="parent"><a href="<?= home_url('cost') ?>">料金</a></li>
                  <li class="parent"><a href="<?= home_url('access') ?>">よくあるご質問</a></li>
                  <li class="parent"><a href="<?= home_url('inquiry') ?>">お問い合わせ</a></li>
            </ul>
            <div class="mask"></div>
          </nav>
       </div>
   </div>

</header>