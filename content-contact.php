<?php
/*  お問い合わせ 汎用 */
$tel_number = '03-6263-8163';
?>
<div class="contact-frame">
  <a class="btn tel" href="tel:<?= $tel_number ?>">
    <img
      class="auto"
      src="<?= get_template_directory_uri() ?>/img/h_tel.png"
      alt=""
    />
    <div class="txt">
      <div class="primary"><?= $tel_number ?></div>
      <div>電話受付10:00～17:00</div>
    </div>
  </a>
  <a class="btn mail" href="mail:">
    <img
      class="auto"
      src="<?= get_template_directory_uri() ?>/img/h_mail.png"
      alt=""
    />
    <div class="txt">
      <div>24時間受付中</div>
      <div class="primary">WEBご予約</div>
    </div>
  </a>
</div>
