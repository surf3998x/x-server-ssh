<?php
/**
 * お問い合わせ
 */
?>


<article class="contact">
	<header>
		<h2>お住いに関するお困りごとはお気軽にご相談ください。</h2>
	</header>
	<p class="description"> お住いに関するお困りごとはどんなことでもご相談ください。<br>
		お急ぎの場合はお電話でのお問い合わせください。​迅速にご対応いたします。</p>

	<dl class="text-center">
		<dt>
			<aside>
				お電話：03-4455-9067 &nbsp; &nbsp; FAX：050-3737-5237 &nbsp; &nbsp; メール：info@juuhokyou.net</aside>
		</dt>
		<dd>
			<address>〒105-0014 東京都港区芝２丁目５番地１９号
				一般社団法人 住環境保全補強協会（住補協）</address>
		</dd>
	</dl>
	<div class="clearfix">
		<?php echo do_shortcode('[contact-form-7 id="21" title="お問い合わせフォーム"]'); ?>
	</div>
</article>