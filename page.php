<?php
/**
 *  固定ページ
 */
get_header();
?>

<div id="main-visual" class="top">
  <header>
    <h1><?= the_title() ?></h1>
  </header>
</div>

<div class="breadcrumb-wrap">
  <div class="container">
    <?php the_breadcrumb();?>
  </div>
</div>

<main id="main-lauout" class="">
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <article>
    <section>
      <div class="container">
        <header>
          <h1 class="h1 text-center"><?php the_title() ?></h1>
        </header>

        <?php the_content(); ?>
      </div>
    </section>
  </article>

  <?php endwhile; endif; ?>
</main>

<?php get_footer(); ?>
