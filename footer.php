
<footer id="footer">
    <div id="return_top" class="active">
        <a href="#">
          <img src="<?= get_template_directory_uri() ?>/img/patge-top.png" alt="">
        </a>
    </div>
  <div class="container">

    <div class="logo text-center">
        <img src="<?= get_template_directory_uri() ?>/img/site_logo.svg" alt="サイトロゴ">
    </div>
        <div class="flex-footer">
          <div class="flex-row left">
          <ul class="nav">
              <li class="parent"><a href="<?= home_url('about') ?>">クリック紹介</a>
                  <ul class="sub-menu acc-child-close">
                    <li class="menu-item">
                      <a href="<?= home_url('about') ?>#message">ご挨拶</a>
                    </li>
                    <li class="menu-item">
                      <a href="<?= home_url('about') ?>#introducing">院長紹介</a>
                    </li>
                    <li class="menu-item">
                      <a href="<?= home_url('about') ?>#room">院内施設のご紹介</a>
                    </li>
                  </ul>
              </li>

              <li class="parent"><a href="<?= home_url('cancer') ?>">がん治療について</a>
                 <ul class="sub-menu acc-child-close">
                    <li class="menu-item">
                      <a href="<?= home_url('cancer') ?>#problem">がん標準治療の問題点</a>
                    </li>
                    <li class="menu-item">
                      <a href="<?= home_url('cancer') ?>#structure">がん細胞の増える仕組みと解決策</a>
                    </li>
                    <li class="menu-item">
                      <a href="<?= home_url('cancer') ?>#solution">がん免疫活性化の鍵は樹上細胞にあり</a>
                    </li>
                    <li class="menu-item">
                      <a href="<?= home_url('cancer') ?>#necessary">免疫力の強化が必要不可欠</a>
                    </li>
                  </ul>
              </li>
         </div>
         <div class="flex-row center">
             <ul class="nav">
                  <li class="parent"><a href="<?= home_url('treatment') ?>">当院のがん治療</a>
                      <ul class="sub-menu acc-child-close">
                        <li class="menu-item">
                          <a href="<?= home_url('treatment') ?>#vaccine">WT1樹状細胞ワクチン療法とは</a>
                        </li>
                        <li class="menu-item">
                          <a href="<?= home_url('treatment') ?>#lyphocyte">活性化リンパ球療法</a>
                        </li>
                        <li class="menu-item">
                          <a href="<?= home_url('treatment') ?>#inhibitor">免疫チェックポイント阻害剤</a>
                        </li>
                      </ul>
                  </li>
                  <li class="parent"><a href="<?= home_url('flow') ?>">治療の流れ</a>
                      <ul class="sub-menu acc-child-close">
                        <li class="menu-item">
                          <a href="<?= home_url('flow') ?>#flow">治療の流れ</a>
                        </li>
                        <li class="menu-item">
                          <a href="<?= home_url('flow') ?>#document">診療に必要な書類・資料</a>
                        </li>
                      </ul>
                  </li>
             </ul>
         </div>
         <div class="flex-row right">
              <ul class="nav">
                  <li class="parent"><a href="<?= home_url('case') ?>">症例・治験状況</a>
                      <ul class="sub-menu acc-child-close">
                        <li class="menu-item">
                          <a href="<?= home_url('case') ?>#case1">症例１</a>
                        </li>
                        <li class="menu-item">
                          <a href="<?= home_url('case') ?>#case2">症例２</a>
                        </li>
                        <li class="menu-item">
                          <a href="<?= home_url('case') ?>#case">症例実績</a>
                        </li>
                        <li class="menu-item">
                          <a href="<?= home_url('case') ?>#clinical_trial">治験状況</a>
                        </li>
                      </ul>
                  </li>               
                  <li class="parent"><a href="<?= home_url('cost') ?>">料金</a></li>
                  <li class="parent"><a href="<?= home_url('access') ?>">よくあるご質問</a></li>
                  <li class="parent"><a href="<?= home_url('inquiry') ?>">お問い合わせ</a></li>
             </ul>
          </div>
      </div>
    </div>

    <div class="footer-copy">
      <div class="container">
          <div class="copy"> © 2021 Ginza Phoenix Clinic</div>
      </div>
    </div>

</footer>
<?php wp_footer(); ?>
</body>
</html>
