<?php
/**
 *  クリニック紹介
 */
get_header();
?>
<div id="main-visual" class="top">
  <header>
    <h1><?= the_title() ?></h1>
  </header>
</div>

<div class="breadcrumb-wrap">
  <div class="container">
    <?php the_breadcrumb();?>
  </div>
</div>

<main id="main-lauout">
  <div style="color: red; text-align: center">[作業中...]</div>
  <section class="context none">
    <div class="container">
      <div class="bg-img about bg">
        <header class="content about">
          <h1 class="text-center">初めてご来院される患者さまへ</h1>
        </header>

        <article class="read flex-h start">
          <div class="con">
            <p>
              高齢化社会に伴い誰もがかなり高い確率でがんに罹患する時代になりました。
              <br />
              がんの標準療法は一定の効果を認める一方で大きなデメリットを持つことが課題となっています。一つは副作用です。がんの標準療法には手術、放射線療法、抗がん剤の3つがありますが、いずれも健常な身体機能を損なう恐れがあり、それには大きな個人差があります。<br />
              もう一つは画一的な治療法であることです。保険診療が適応されている標準療法は、特定の治療方法について大規模な臨床試験（治験）を経て統計学的に効果と副作用を解析し、その方法と結果を根拠にガイドラインを作成し患者さまに治療を提供しています。そのため画一的な治療方法にならざるをえません。患者さま毎に自由に変えることは許容されていません。
            </p>

            <p>
              しかし、がん細胞は増殖しながら遺伝子変異を絶えず繰り返しています。ウイルスの変異と同じように時々刻々と性質が変わります。同じ種類のがん（例えば膵癌）であっても実際は患者さまごとにがん細胞の性質は大きく異なり、またそれも時間と共に変化して多種多様な遺伝子変異を持つがん細胞の集塊を形成していきます。したがって、がんに対して画一的な治療法を実施することは、がんの細胞生物学的な特性からして本来的に限界があります。理想的ながん治療は患者さま一人ひとりの状態に合わせた「個別化医療」を実施することです。
            </p>

            <p>
              個別化医療は現在のところ保険診療では認められていませんが、自由診療の枠では認められています。当院で行っているWT1樹状細胞ワクチン療法などのがん免疫療法は、患者さまの血液から免疫細胞を採取し、細胞加工施設で培養・成熟させて再び患者さまの体内に戻す方法です。患者さま毎にその方専用の細胞製剤を作製しますので究極の個別化医療となります。患者さまの免疫状態、栄養状態、その他の治療法の状況などに合わせて最適な治療を提供できるのが個別化医療の最大のメリットです。
            </p>

            <p>
              また、患者さまご自身の細胞を使う治療法は「再生医療」と呼ばれる分野に属しており、この治療を行うためには再生医療法という法律に遵守して実施しなければなりません。そのためには厚生労働省が指定した認定再生医療等委員会に提供計画を申請し、治療の妥当性や安全性、医療を提供する医師の体制、細胞の培養工程や管理体制など、総合的かつ厳正な審査を受ける必要があります。そこで条件を満たし厚生労働大臣による審査を通過することで初めて医療を実施することができるようになります。したがって自由診療の枠内ではありますが、極めて厳格な再生医療法の範疇で実施される個別化医療であることにより患者さまの安全と安心を担保する仕組みになっています。
            </p>
            <p>
              がんは非常に多彩な疾患なので単一の治療法で克服するのは困難です。保険診療や自由診療の良いところを組み合わせて集学的に治療していくことが大切です。当院では患者さま各々の状況に合わせてWT1樹状細胞ワクチン療法を中心としたがん免疫療法を提供し、理想的な個別化医療の実践を目指しています。がんの患者様に少しでも希望を持っていただけるよう最大の努力を尽くしたいと考えています。是非ご相談ください。
            </p>
          </div>
          <div>
            <figure class="inchou">
              <img
                class="auto"
                src="<?= get_template_directory_uri() ?>/img/about/inchou_face.png"
                alt=""
              />
              <dl>
                <dt class="mfont">銀座鳳凰クリニック</dt>
                <dd>
                  <div class="person">
                    <p class="mfont"><small>院長</small>永 井 恒 志</p>
                    <small class="kana mfont">Hisashi Nagai</small>
                  </div>
                </dd>
              </dl>
            </figure>
          </div>
        </article>
      </div>
    </div>

    <article>
      <header class="content">
        <h2 class="h1 text-center">院長紹介</h2>
      </header>
      <div class="profile flex-h start">
        <dl class="left">
          <dt>専門</dt>
          <dd>
            <p>がん免疫療法</p>
            <p>再生医療</p>
            <p>抗加齢医学</p>
          </dd>
        </dl>
        <dl class="mid">
          <dt>経歴</dt>
          <dd>
            <p><span>2003年</span> 金沢医科大学医学部医学科卒業</p>
            <p><span>2003年</span> 東京大学医学部附属病院内科研修医</p>
            <p>
              <span>2004年</span> 東京女子医科大学医学部第一内科学教室研究医
            </p>
            <p><span>2010年</span> 東京大学大学院医学系研究科助教</p>
            <p><span>2015年</span> 医学博士号取得（東京大学）</p>
            <p><span>2016年</span> 慶應義塾大学医学部非常勤講師</p>
            <p><span>2018年</span> 東海大学大学院人間環境学研究科客員准教授</p>
            <p><span>2021年</span> 銀座鳳凰クリニック院長</p>
          </dd>
        </dl>
        <dl class="right">
          <dt>所属学会</dt>
          <dd>
            <p>日本再生医療学会</p>
            <p>日本臨床免疫学会</p>
            <p>日本がん免疫学会</p>
            <p>日本抗加齢医学会</p>
            <p>日本統合医療学会</p>
            <p>国際個別化医療学会</p>
          </dd>
        </dl>
      </div>
    </article>
  </section>

  <section class="gray-white context none">
    <div class="container">
      <div class="bg-img room bg">
        <header class="content about">
          <h1 class="text-center">院内施設のご紹介</h1>
        </header>
        <p class="read">
          当院は全ての空間において患者様のプライバシーを守りながら安全かつ安心して快適に治療を受けていただけるように配慮しています。全室とも窓を開放できるようにしており、常に良好な換気ができるように工夫しています。完全予約制・完全個室によりストレスなくご相談・ご受診いただけます。
        </p>
        <div class="flex-h panel clearfix">
          <div class="flex-colum">
            <figure>
              <img
                src="<?= get_template_directory_uri() ?>/img/about/about_1.png"
                alt=""
              />
              <figcaption class="key-bgcolor">ビル</figcaption>
            </figure>
          </div>
          <div class="flex-colum">
            <figure>
              <img
                src="<?= get_template_directory_uri() ?>/img/about/about_2.png"
                alt=""
              />
              <figcaption class="key-bgcolor">受付・待合室</figcaption>
            </figure>
          </div>
          <div class="flex-colum">
            <figure>
              <img
                src="<?= get_template_directory_uri() ?>/img/about/about_3.png"
                alt=""
              />
              <figcaption class="key-bgcolor">診察室</figcaption>
            </figure>
          </div>
          <div class="flex-colum">
            <figure>
              <img
                src="<?= get_template_directory_uri() ?>/img/about/about_4.png"
                alt=""
              />
              <figcaption class="key-bgcolor">成分採決室</figcaption>
            </figure>
          </div>
          <div class="flex-colum">
            <figure>
              <img
                src="<?= get_template_directory_uri() ?>/img/about/about_5.png"
                alt=""
              />
              <figcaption class="key-bgcolor">相談室</figcaption>
            </figure>
          </div>
          <div class="flex-colum">
            <figure>
              <img
                src="<?= get_template_directory_uri() ?>/img/about/about_6.png"
                alt=""
              />
              <figcaption class="key-bgcolor">点滴室</figcaption>
            </figure>
          </div>
        </div>
      </div>
    </div>
    <div class="white-box">
      <!-- 共通お問い合わせ -->
      <?php echo get_template_part('content','contact_info'); ?>
    </div>
  </section>
</main>

<?php get_footer(); ?>
