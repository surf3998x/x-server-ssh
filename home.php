<?php
get_header();
$tel_number = '03-6263-8163';
?>

<div id="main-visual" class="home"></div>

<main id="main-lauout">
  <section class="context none">
    <div class="container">
      <header class="home content">
        <h1 class="key-color text-center">銀座鳳凰クリニックへようこそ</h1>
        <p class="read">
          銀座鳳凰クリニックは厚生労働省により第三種再生医療の提供を認可されたがん免疫療法の専門医療機関です。患者さま一人ひとりに合わせた個別化医療でがんと闘います。
        </p>
      </header>
      <article>
        <div class="corona flex-h">
          <div class="box blue">
            <header>
              <h2 class="text-center">
                新型コロナウイルス感染症 <br />
                への取り組み
              </h2>
            </header>
            <a class="btn" href="<?= home_url('') ?>"
              ><span class="arrow">詳細はこちら</span></a
            >
          </div>
          <div class="box white">
            <img
              class="auto"
              src="<?= get_template_directory_uri() ?>/img/site_logo.svg"
              alt=""
            />
          </div>
        </div>
      </article>
    </div>
  </section>

  <section class="gray context">
    <div class="container">
      <div class="bg-img about bg">
        <header class="content home">
          <h1 class="text-center">銀座鳳凰クリニックとは</h1>
        </header>
      </div>
    </div>

    <div class="bg-img pos sec-1">
      <figure class="bg-right full">
        <img
          src="<?= get_template_directory_uri() ?>/img/home/sec_1.png"
          alt=""
        />
      </figure>
      <div class="box-left white">
        <header>
          <h2 class="key-color">
            当院には、このようなお悩みをお持ちの患者様が多く来院されています。
          </h2>
        </header>
        <p>
          がんは今でも治療の難しい病気です。 <br />
          特に末期がんは患者さまの体力の低下などから標準治療ができない場合も少なくありません。<br />
          しかし諦めてはいけません。<br />
          がんに対する治療法は日々進化しています。
        </p>
        <p>
          私たちはがん免疫学の専門家として「免疫こそ最大の武器である」ことを確信しています。当院には下記のようなお悩みをお持ちの患者様から多くのご相談をいただいています。<br />
          15年以上の経験と実績で確立された免疫療法をひとりでも多くのがん患者のみなさまにお届けしたいと願っています。
        </p>
      </div>
    </div>

    <ul class="number-list flex-h">
      <li>
        <h2 class="number key-color">
          <span class="num">01</span>
          <span>がんと診断され最適な治療を考えている方</span>
        </h2>
        <img
          src="<?= get_template_directory_uri() ?>/img/home/sec_1_1.png"
          alt=""
        />
        <p>
          WT1樹状細胞ワクチン療法をはじめとするがん免疫療法は他の治療法と併用することが可能です。がん免疫療法が他の治療法を制限したり阻害したりすることはありません。StageⅠからStageⅣまでお体の状態や免疫機能が問題なければお受け頂くことができます。私どもは早期がんであってもがん特異的免疫機構を強化して正常細胞を傷つけることなく細胞レベルでがんを攻撃しておくべきであると考えています。
        </p>
      </li>
      <li class="border">
        <h2 class="number key-color">
          <span class="num">02</span>
          <span>がんが再発・転移してしまい悩まれている方</span>
        </h2>
        <img
          src="<?= get_template_directory_uri() ?>/img/home/sec_1_2.png"
          alt=""
        />
        <p>
          WT1樹状細胞ワクチン療法は免疫によってがん細胞を攻撃する治療法ですので、がんの発生部位を基本的に問いません。がんが再発あるいは転移している場合には画像診断で映らないような微小がんも複数存在する可能性があります。免疫細胞はこのような微小がんでも攻撃することができます。
        </p>
      </li>
      <li>
        <h2 class="number key-color">
          <span class="num">03</span>
          <span>有効な治療法がなく緩和ケアを検討されている方</span>
        </h2>
        <img
          src="<?= get_template_directory_uri() ?>/img/home/sec_1_3.png"
          alt=""
        />
        <p>
          一般的ながん治療においては標準療法の適応がない状態となった場合は緩和ケアを勧められることが多いのが現状です。しかしWT1樹状細胞ワクチン療法はそのような患者さまでもお受け頂くことが可能です。免疫がしっかりと働いてくれる可能性があればWT1樹状細胞ワクチン療法が奏功する可能性があります。私どもは極めて厳しい状態であっても可能な限り全力を尽くして治療に当たっています。
        </p>
      </li>
    </ul>

    <ul class="number-list flex-h center">
      <li>
        <h2 class="number key-color">
          <span class="num">04</span>
          <span>抗がん剤治療の副作用に悩まれている方</span>
        </h2>
        <img
          src="<?= get_template_directory_uri() ?>/img/home/sec_1_4.png"
          alt=""
        />
        <p>
          抗がん剤が有効な治療法であることは医学界においてコンセンサスを得ています。しかし抗がん剤全般に共通していることはやはり副作用の問題です。副作用によって途中で抗がん剤を中止せざるを得ないこともしばしばあります。WT1樹状細胞ワクチン療法は患者さま固有の免疫細胞を使用してワクチン製剤を作製する個別化医療なので抗がん剤のような副作用は出現しません。がん治療における副作用でお悩みの患者さまにも適した治療法です。
        </p>
      </li>
      <li class="border-left">
        <h2 class="number key-color">
          <span class="num">05</span>
          <span>経過観察中でがんの再発予防をお考えの方</span>
        </h2>
        <img
          src="<?= get_template_directory_uri() ?>/img/home/sec_1_5.png"
          alt=""
        />
        <p>
          がんの怖さのひとつが再発です。治療により画像検査でがんを描出しなくなり、腫瘍マーカーも極めて低い値で安定していれば寛緩していると判断しますが、これはがん細胞がゼロになったことを意味しているのではありません。そのまま完治となるのか、あるいは再発するのかは経過観察していかなければ分からないのです。治療により寛緩した時にWT1樹状細胞ワクチン療法を行うことによってがんの再発を阻止できる可能性があります。がんの再発予防医療としてWT1樹状細胞ワクチン療法は非常に有望であると私どもは考えています。
        </p>
      </li>
    </ul>

    <div class="text-center ptb">
      <a class="btn" href="<?= home_url('about') ?>">
        <span class="arrow">クリック紹介</span></a
      >
    </div>
  </section>

  <section class="white context">
    <div class="container">
      <div class="bg-img problem bg">
        <header class="content home">
          <h1 class="text-center">がん標準治療の問題点</h1>
        </header>
        <p class="read">
          手術、抗がん剤、放射線療法をがんの標準治療と言います。 <br />
          一定の効果がある反面、副作用が強い、効果がなくなる、免疫機能を低下させる、などいくつもの大きな問題をかかえています。
        </p>
      </div>
    </div>
    <div class="problem container">
      <div class="list flex-h fix">
        <div class="flex-colum">
          <figure>
            <figcaption class="key-color mfont">抗がん剤</figcaption>
            <img
              src="<?= get_template_directory_uri() ?>/img/home/sec_2_1.png"
              alt=""
            />
          </figure>
          <p>化学物質によってがん細胞の増殖を抑える</p>
        </div>
        <div class="flex-colum">
          <figure>
            <figcaption class="key-color mfont">放射線治療</figcaption>
            <img
              src="<?= get_template_directory_uri() ?>/img/home/sec_2_2.png"
              alt=""
            />
          </figure>
          <p>放射線を浴びせて遺伝子を破壊する</p>
        </div>
        <div class="flex-colum">
          <figure>
            <figcaption class="key-color mfont">手術</figcaption>
            <img
              src="<?= get_template_directory_uri() ?>/img/home/sec_2_3.png"
              alt=""
            />
          </figure>
          <p>がんを出来るだけ切除して取り除く</p>
        </div>
      </div>
    </div>
    <div class="text-center ptb">
      <a class="btn" href="<?= home_url('cancer') ?>"
        ><span class="arrow">がん治療について</span></a
      >
    </div>
  </section>

  <section class="gray context vaccine">
    <div class="container">
      <div class="bg-img vaccine bg">
        <header class="content home">
          <h1 class="text-center">WT1樹状細胞ワクチン療法</h1>
        </header>
      </div>
    </div>

    <div class="full-box">
      <header>
        <h2 class="key-color text-center">WT1樹状細胞ワクチン療法とは</h2>
      </header>
      <div class="flex-h reverse">
        <figure>
          <img
            class="auto"
            src="<?= get_template_directory_uri() ?>/img/home/sec_3.png"
            alt=""
          />
        </figure>
        <p>
          樹状細胞は、がんを攻撃するリンパ球にがんだけが持つ目印（がん抗原）を教える枝のような突起(樹状突起)を持つ免疫細胞です。<br />
          近年、がんが樹状細胞を弱らせてがん抗原を認識できないようにすることでがん細胞を免疫細胞が攻撃できないようにしていることが分かりました。<br />
          つまり樹状細胞が弱っていることが、がんに対して免疫が働かない第一の要因なのです。<br />
          そこで人工がん抗原であるWT1を樹状細胞も認識させ、T細胞に対する抗原提示能を活性化させた、いわば強い樹状細胞を患者さまの体内に送り込むことが必要なのです。
          <br />
          これがWT1樹状細胞ワクチン療法です。
        </p>
      </div>
    </div>

    <div class="flex-h panel">
      <div class="flex-colum">
        <figure>
          <img
            src="<?= get_template_directory_uri() ?>/img/home/vaccine1.jpg"
            alt=""
          />
          <figcaption class="key-bgcolor">
            単球を患者さんの血液から採取
          </figcaption>
        </figure>
        <p>
          樹状細胞の元となる単球を患者さんの血液から採取し、細胞加工施設で成熟した樹状細胞に成長させます。
        </p>
      </div>
      <div class="flex-colum">
        <figure>
          <img
            src="<?= get_template_directory_uri() ?>/img/home/vaccine2.jpg"
            alt=""
          />
          <figcaption class="key-bgcolor">
            樹状細胞にがん抗原WT1を与えます
          </figcaption>
        </figure>
        <p>
          樹状細胞に人工的に作られたがんの目印であるがん抗原WT1を与えます。<br />WT1を手に入れた樹状細胞は、リンパ球にWT1を教えることができる一人前の司令塔になります。
        </p>
      </div>
      <div class="flex-colum">
        <figure>
          <img
            src="<?= get_template_directory_uri() ?>/img/home/vaccine3.jpg"
            alt=""
          />
          <figcaption class="key-bgcolor">
            WT1樹状細胞ワクチンとして注射
          </figcaption>
        </figure>
        <p>
          司令塔に育ったたくさんの樹状細胞を「WT1樹状細胞ワクチン」として注射します。
        </p>
      </div>
    </div>
    <div class="flex-h center panel">
      <div class="flex-colum">
        <figure>
          <img
            src="<?= get_template_directory_uri() ?>/img/home/vaccine4.jpg"
            alt=""
          />
          <figcaption class="key-bgcolor">樹状細胞ががん細胞を攻撃</figcaption>
        </figure>
        <p>
          体に入った樹状細胞は免疫の司令塔としてリンパ球にWT1を教え、がん細胞を攻撃するように指導します。
        </p>
      </div>
      <div class="flex-colum">
        <figure>
          <img
            src="<?= get_template_directory_uri() ?>/img/home/vaccine5.jpg"
            alt=""
          />
          <figcaption class="key-bgcolor">
            リンパ球ががん細胞を狙って攻撃
          </figcaption>
        </figure>
        <p>
          WT1を覚えたリンパ球が体中をめぐって、WT1を発現しているがん細胞を狙って攻撃します。
        </p>
      </div>
    </div>
  </section>

  <section class="white context">
    <div class="container">
      <div class="bg-img features bg">
        <header class="content home">
          <h1 class="text-center">WT1樹状細胞ワクチン療法の効果と特長</h1>
        </header>
      </div>
    </div>

    <ul class="number-list flex-h none">
      <li>
        <h2 class="number key-color">
          <span class="num">01</span>
          <span>進行がんでも効果が期待できる</span>
        </h2>
        <img
          src="<?= get_template_directory_uri() ?>/img/home/sec_4_1.png"
          alt=""
        />
        <p>
          これまでWT1樹状細胞ワクチン療法はその多くをStageⅣの患者さまで実施して参りました。いわゆる末期がんであっても効果が得られています。
        </p>
      </li>
      <li class="border">
        <h2 class="number key-color">
          <span class="num">02</span>
          <span>がんを狙い撃ちできるので副作用がほとんどない</span>
        </h2>
        <img
          src="<?= get_template_directory_uri() ?>/img/home/sec_4_2.png"
          alt=""
        />
        <p>
          WT1樹状細胞ワクチン療法はがん細胞に対する特異的免疫反応を獲得し、がん細胞を“狙い撃ち”する治療法です。そのため最も副作用の少ないがん治療法であると言えます。
        </p>
      </li>
      <li>
        <h2 class="number key-color">
          <span class="num">03</span>
          <span>がんに対する免疫力が長期間持続する</span>
        </h2>
        <img
          src="<?= get_template_directory_uri() ?>/img/home/sec_4_3.png"
          alt=""
        />
        <p>
          定期的にワクチンを接種することで免疫記憶を維持し、常にがん細胞を監視し、WT1を発現するがん細胞を見つけたら速やかに細胞死を誘発し増殖を防ぎます。
        </p>
      </li>
    </ul>

    <ul class="number-list flex-h center none">
      <li>
        <h2 class="number key-color">
          <span class="num">04</span>
          <span>他のがん治療法と併用できる</span>
        </h2>
        <img
          src="<?= get_template_directory_uri() ?>/img/home/sec_4_4.png"
          alt=""
        />
        <p>
          WT1樹状細胞ワクチン療法は抗がん剤や放射線療法と並行して実施することが可能です。
        </p>
      </li>
      <li class="border-left">
        <h2 class="number key-color">
          <span class="num">05</span>
          <span>治療は定期的な通院のみ</span>
        </h2>
        <img
          src="<?= get_template_directory_uri() ?>/img/home/sec_4_5.png"
          alt=""
        />
        <p>
          ワクチン製剤が完成したあとはワクチンを皮内注射するために定期的に通院するだけなので治療の継続性に優れています。
        </p>
      </li>
    </ul>

    <div class="text-center ptb">
      <a class="btn" href="<?= home_url('treatment') ?>"
        ><span class="arrow">詳細を見る</span></a
      >
    </div>
  </section>

  <section class="gray context">
    <div class="container">
      <div class="bg-img case bg">
        <header class="content home">
          <h1 class="text-center">WT1樹状細胞ワクチン療法の症例実績</h1>
        </header>
        <div class="flex-h">
          <figure class="pos">
            <figcaption class="h2">
              WT1樹状細胞ワクチン療法におけるがんの種類ごとの症例実績
            </figcaption>
            <img
              class="auto"
              src="<?= get_template_directory_uri() ?>/img/home/sec_5_1.png"
              alt=""
            />
          </figure>
          <figure class="pos">
            <figcaption class="h2">疾患制御率</figcaption>
            <img
              class="max200 auto"
              src="<?= get_template_directory_uri() ?>/img/home/sec_5_2.png"
              alt=""
            />
          </figure>
        </div>
      </div>
      <div class="text-center ptb">
        <a class="btn" href="<?= home_url('case') ?>"
          ><span class="arrow">詳細を見る</span></a
        >
      </div>
    </div>
  </section>

  <section class="white context">
    <div class="container">
      <div class="bg-img question bg">
        <header class="content home">
          <h1 class="text-center">よくある質問</h1>
        </header>
      </div>
    </div>

    <div class="question-list flex-h">
      <div class="flex-column">
        <div class="box flex-h start">
          <span class="key-color mfont">Q</span>
          <p>
            現在治療中なのですが、副作用が強く樹状細胞ワクチン療法に変えたいと思っておりますが、どうでしょうか?
          </p>
        </div>
        <div class="box flex-h start">
          <span class="key-color mfont">Q</span>
          <p>どれくらいで効果があらわれてきますか?</p>
        </div>
      </div>
      <div class="flex-column">
        <div class="box flex-h start">
          <span class="key-color mfont">Q</span>
          <p>私のがんではどのぐらいの効果が期待できますか？</p>
        </div>
        <div class="box flex-h start">
          <span class="key-color mfont">Q</span>
          <p>副作用はありますか?</p>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="text-center ptb">
        <a class="btn" href="<?= home_url('qa') ?>"
          ><span class="arrow">詳細を見る</span></a
        >
      </div>
    </div>

    <!-- 共通お問い合わせ -->
   <?php echo get_template_part('content','contact_info'); ?>
  </section>

  <section class="access gray">
    <div class="container">
      <div class="bg-img access bg">
        <header class="content home">
          <h1 class="text-center">アクセス情報</h1>
        </header>
        <div class="text-center pc-only">
          <img
            src="<?= get_template_directory_uri() ?>/img/site_logo.svg"
            alt=""
          />
        </div>
      </div>
    </div>
    <div class="map flex-h start">
      <figure>
        <img
          src="<?= get_template_directory_uri() ?>/img/home/map.png"
          alt=""
        />
      </figure>
      <div class="access-info">
        <div class="logo text-center sp-only">
          <img
            src="<?= get_template_directory_uri() ?>/img/site_logo.svg"
            alt=""
          />
        </div>
        <p>
          〒104-0061 東京都中央区銀座８丁目８-１ 第７セントラルビル3F <br />
          中央通り銀座七丁目交差点を曲がってすぐ
        </p>
        <dl>
          <dt>最寄り駅</dt>
          <dd>
            <p class="key-color">
              ●東京メトロ銀座線・丸の内線・日比谷線 <br />
              「銀座駅」より徒歩5分
            </p>
            <p class="key-color">
              ●JR山手線 <br />
              「新橋駅」より徒歩5分
            </p>

            <strong>[ 診療時間 ]</strong> <br />
            <span class="key-color">平日・土曜：10:00~17:00</span> <br />
            <strong>[ 休診日 ]</strong> <br />
            <span class="key-color">日曜・祝日</span>
          </dd>
        </dl>
      </div>
    </div>
    <div class="white">
      <div class="container">
        <?php echo get_template_part('content','contact'); ?>
      </div>
    </div>
  </section>
</main>

<?php get_footer(); ?>
