<?php
/**
 *  Template Name: お問い合わせ
 */
get_header();
?>

<div class="breadcrumb-wrap">
  <div class="container">
    <?php the_breadcrumb();?>
  </div>
</div>

<div id="main-visual" class="contact"></div>

<main id="main-lauout" class="contact">
  <article>
    <section>
      <div class="container">
        <header>
          <h1 class="h1 text-center">お問い合わせフォーム</h1>
        </header>
        <div class="read-box">
          <p class="read">
            弊社では電話 ・ E-Mail
            ・フォームによるお問い合わせを受け付けております。 <br />
            問い合わせ先は以下の通りとなっております。
          </p>
          <div class="flex">
            <span>電話によるお問い合わせ</span><span>: 03-6432-4963</span>
          </div>
          <div class="flex">
            <span>E-Mailによるお問い合わせ</span><span>: info@zeevaa.com</span>
          </div>
          <div class="flex">
            <span>フォームによるお問い合わせ</span
            ><span
              >:
              下記のフォームから必要項目をご入力の上、お気軽にご連絡下さい。</span
            >
          </div>
        </div>
        <div class="wrap">
          <?php
            echo do_shortcode('[contact-form-7 id="48" title="お問い合わせフォーム"]');
          ?>
          <p class="cyui key-color">
            ※ E-Mail ・ フォームによるお問い合わせの場合、回答までに最大で土日
            ・ 祝日を除いた2営業日程度を頂いております。 <br />
            ※ 回答が届かない場合、【 info@zeevaa.com
            】までご連絡をお願いいたします。
          </p>
        </div>
      </div>
    </section>
  </article>
</main>

<?php get_footer(); ?>
<script>
  document.addEventListener( 'wpcf7mailsent', function( event ) {

      location = '<?= home_url('contact-thanks') ?>';

  }, false );
</script>
