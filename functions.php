<?php
// アイキャッチ画像を有効にする。
add_theme_support('post-thumbnails'); 

// ウィジェット
register_sidebar(array(
     'name' => 'sidebar' ,
     'id' => 'side' ,
     'before_widget' => '<div class="widget">',
     'after_widget' => '</div>',
     'before_title' => '<h3>',
     'after_title' => '</h3>'
));


//HTML5でのマークアップで出力する
add_theme_support( 'html5',array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
) );

//ヘッダに勝手に挿入されるコードを消す
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action( 'wp_head', 'wp_generator' );
function remove_recent_comments_style() {
    global $wp_widget_factory;
    remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'remove_recent_comments_style' );


//絵文字対応スクリプトを削除する
function disable_emoji() {
     remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
     remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
     remove_action( 'wp_print_styles', 'print_emoji_styles' );
     remove_action( 'admin_print_styles', 'print_emoji_styles' );
     remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
     remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
     remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', 'disable_emoji' );



//メディアを記事に挿入する際、クラス名を書き込まない
function remove_img_attr($html, $id, $alt, $title, $align, $size) {
    return preg_replace('/ class=[\'"]([^\'"]+)[\'"]/i', '', $html);
}
add_filter('get_image_tag','remove_img_attr', 10, 6);



// スマホのみ ※タブレット対象外
function is_mobile(){
$useragents = array(
'iPhone', // iPhone
'iPod', // iPod touch
'Android.*Mobile', // 1.5+ Android *** Only mobile
'Windows.*Phone', // *** Windows Phone
'dream', // Pre 1.5 Android
'CUPCAKE', // 1.5+ Android
'blackberry9500', // Storm
'blackberry9530', // Storm
'blackberry9520', // Storm v2
'blackberry9550', // Storm v2
'blackberry9800', // Torch
'webOS', // Palm Pre Experimental
'incognito', // Other iPhone browser
'webmate', // Other iPhone browser
'Nexus 6',
'Nexus 5',
'Nexus 4',
);
$pattern = '/'.implode('|', $useragents).'/i';
return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}


// JS・CSSファイルを読み込む
function add_files() {

   wp_deregister_script('jquery');
// サイト共通JS
    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-3.2.1.min.js');
    wp_enqueue_script( 'scroll', get_template_directory_uri() . '/js/scroll.js');
    wp_enqueue_script( 'meanmenu', get_template_directory_uri() . '/js/jquery.meanmenu.min.js');
    wp_enqueue_script( 'flexibility', get_template_directory_uri() . '/js/flexibility.js');

// サイト共通CSSの読み込み
  wp_enqueue_style( 'meanmenu', get_template_directory_uri() . '/css/meanmenu.css' );
  wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );


}
add_action('wp_enqueue_scripts', 'add_files');




function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );



//抜粋pタグ削除
remove_filter('the_excerpt', 'wpautop');


//記事一覧抜粋の文字数変更
function new_excerpt_mblength() {
return 50;
}
add_filter('excerpt_mblength', 'new_excerpt_mblength');


//---------------------------------------------------------------------------
//  カスタムメニューで自動出力されるタグからulを削除
//---------------------------------------------------------------------------
function remove_ul ( $menu ){
    return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );
}
add_filter( 'wp_nav_menu', 'remove_ul' );

//カスタムメニュー
if ( ! function_exists( 'lab_setup' ) ) :
function lab_setup() {
  register_nav_menus( array(
    'mainmenu' => 'メインメニュー',
    'footermenu' => 'フッターメニュー'
  ) );
}
endif;
add_action( 'after_setup_theme', 'lab_setup' );

function pagename_class($classes = []) {
  if( is_page() ) {
    $page = get_post(get_the_ID());
    $classes[] = 'body_' . $page->post_name;
  }
  return $classes;
}
add_filter('body_class', 'pagename_class');

function getPageCSSFilePath(){
  if( is_home() || is_front_page() ){

    return get_template_directory_uri() . '/css/home.css';

  }
  if( is_page() ) {
    $page = get_post(get_the_ID());
    return sprintf('%s/css/%s.css',get_template_directory_uri(),$page->post_name);
  }

  if( is_single() ) {
    $page = get_post(get_the_ID());
    return '/css/single.css';
  }
  
}




add_filter('nav_menu_item_id', 'removeId', 10);
function removeId( $id ){
    return $id = array();
}


function add_meta_query_vars( $public_query_vars ) {
    $public_query_vars[] = 'nav';
    return $public_query_vars;
}
add_filter( 'query_vars', 'add_meta_query_vars' );


//カテゴリースラッグからリンク取得
 function get_cat_link( $slug = '' ){
      $term = get_term_by('slug' , $slug , 'category');
      return get_term_link( $term->term_id );
 }




/* パンくず
* ---------------------------------------- */

  function the_breadcrumb(){

    global $post;
    // ポストタイプを取得
    $post_type = get_post_type( $post );

    $bc  = '<ol class="breadcrumb clearfix">';
    $bc .= '<li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.home_url().'" itemprop="url"> <span itemprop="title">HOME</span></a></li>';

    if( is_home() || is_front_page() ){
      // メインページ
     // $bc .= '<li> 最新記事一覧</li>';
    }elseif( is_search() ){
      // 検索結果ページ
      $bc .= '<li> 「'.get_search_query().'」の検索結果</li>';
    }elseif( is_404() ){
      // 404ページ
      $bc .= '<li> ページが見つかりませんでした</li>';
    }elseif( is_date() ){
      /*
      // 日付別一覧ページ
      $bc .= '<li> ';
      if( is_day() ){
        $bc .= get_query_var( 'year' ).'年 ';
        $bc .= get_query_var( 'monthnum' ).'月 ';
        $bc .= get_query_var( 'day' ).'日';
      }elseif( is_month() ){
        $bc .= get_query_var( 'year' ).'年 ';
        $bc .= get_query_var( 'monthnum' ).'月 ';
      }elseif( is_year() ){
        $bc .= get_query_var( 'year' ).'年 ';
      }
      $bc .= '</li>';
      */
    }elseif( is_tax() ){

      $cat = get_queried_object();
      if( $cat->taxonomy == 'ballet_tag'){
         $bc .= '<li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.home_url('ballet') .'" itemprop="url"> <span itemprop="title">バレエ教室</span></a></li>';
      }

      $bc .= '<li> '.single_term_title( '', false ).'</li>';

    }elseif( is_post_type_archive() ){

      $obj = get_queried_object();

      // カスタムポストアーカイブ
      $bc .= '<li> '. $obj->labels->singular_name .'</li>';

    }elseif( is_category() ){
      // カテゴリーページ
      $cat = get_queried_object();
      if( $cat -> parent != 0 ){
        $ancs = array_reverse(get_ancestors( $cat->cat_ID, 'category' ));
        foreach( $ancs as $anc ){
          $bc .= '<li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.get_category_link($anc).'" itemprop="url"> <span itemprop="title">'.get_cat_name($anc).'</span></a></li>';
        }
      }
      $bc .= '<li> '.$cat->cat_name.'</li>';
    }elseif( is_tag() ){
      // タグページ
      $bc .= '<li>'.single_tag_title("",false).'</li>';

    }elseif( is_author() ){
      // 著者ページ
      $bc .= '<li> '.get_the_author_meta('display_name').'</li>';

    }elseif( is_attachment() ){
      // 添付ファイルページ
      if( $post->post_parent != 0 ){
        $bc .= '<li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.get_permalink( $post->post_parent ).'" itemprop="url"> <span itemprop="title">'.get_the_title( $post->post_parent ).'</span></a></li>';
      }
      $bc .= '<li> '.$post->post_title.'</li>';

    }elseif( is_singular('post') ){
      $cats = get_the_category( $post->ID );
      $cat = $cats[0];

      if( $cat->parent != 0 ){
        $ancs = array_reverse(get_ancestors( $cat->cat_ID, 'category' ));
        foreach( $ancs as $anc ){
          $bc .= '<li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.get_category_link( $anc ).'" itemprop="url"> <span itemprop="title">'.get_cat_name($anc).'</span></a></li>';
        }
      }
      $bc .= '<li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.get_category_link( $cat->cat_ID ).'" itemprop="url"> <span itemprop="title">'.$cat->cat_name.'</span></a></li>';
      $bc .= '<li> '.$post->post_title.'</li>';

    }elseif( is_singular('page') ){
      // 固定ページ
      if( $post->post_parent != 0 ){
        $ancs = array_reverse( $post->ancestors );
        foreach( $ancs as $anc ){
          $bc .= '<li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.get_permalink( $anc ).'" itemprop="url"><span itemprop="title">'.get_the_title($anc).'</span></a>';
        }
      }
      $bc .= '<li> '.$post->post_title.'</li>';

    }elseif( is_singular( $post_type ) ){
      // カスタムポスト記事ページ
      $obj = get_post_type_object($post_type);

      if( $obj->has_archive == true ){
      $bc .= '<li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="'.get_post_type_archive_link($post_type).'" itemprop="url"> <span itemprop="title">'.$obj->labels->singular_name .'</span></a></li>';
      }
      $bc .= '<li> '.$post->post_title.'</li>';

    }else{
      // その他のページ
      $bc .= '<li> '.$post->post_title.'</li>';
    }

    $bc .= '</ol>';

    echo $bc;

  }


 // カスタム投稿
function create_post_type() {

  remove_post_type_support('works','revisions');
  remove_post_type_support('works','custom-fields');
  remove_post_type_support('works','page-attributes');
  remove_post_type_support('works','post-formats');

  $labels = array(
    'name'               => '新着情報',
    'singular_name'      => '新着情報',
    'add_new'            => '新規作成',
    'add_new_item'       => '新着情報を追加',
    'edit_item'          => '新着情報を編集',
    'new_item'           => '新着情報',
    'all_items'          => '新着情報一覧',
    'menu_name'          => '新着情報'
  );
  $args = array(
    'labels'              => $labels,
    'publicly_queryable' =>  true,
    'public'              => true, 
    'show_ui'             => true,
    'query_var'           => false,
    'hierarchical'        => false,
    'menu_position'       => 5,
    'has_archive'         => true,
    'map_meta_cap'        => true,
    'show_in_menu'        => true,
    'capability_type'     => 'post',
    'supports'            => array(
      'title',
      'thumbnail',
      'editor',
      'author',
      'excerpt'
      )
  );
  register_post_type( 'works' , $args);
  flush_rewrite_rules( false );
}
//add_action( 'init', 'create_post_type' );


function searchFilter($query) {

    if ( is_admin() || ! $query->is_main_query() ) return;




}
//add_action( 'pre_get_posts','searchFilter' );






add_filter( 'wpcf7_validate_email*', 'wpcf7_validate_email_filter_extend_confirm', 11, 2 );
function wpcf7_validate_email_filter_extend_confirm( $result, $tag ) {
    $type = $tag['type'];
    $name = $tag['name'];

     $reg_str = "/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/";

    if ( $name == 'your_email' && ('email' == $type || 'email*' == $type) ) {

        $_POST['your_email'] = trim( strtr( (string) $_POST['your_email'], "n", " " ) );

        if ( preg_match($reg_str, $_POST['your_email'] == false) ) {
            if (method_exists($result, 'invalidate')) {
                $result->invalidate( $tag,"不正なメールアドレスが入力されています");
            } else {
                $result['valid'] = false;
                $result['reason']['your_email'] = '不正なメールアドレスが入力されています';
            }
        }

    }

    if ( $name == 'your_email_comfirm' && ('email' == $type || 'email*' == $type) ) {
       $_POST['your_email'] = trim( strtr( (string) $_POST['your_email'], "n", " " ) );
       $_POST['your_email_comfirm'] = trim( strtr( (string) $_POST['your_email_comfirm'], "n", " " ) );
            $target_name = $matches[1];
            if ($_POST['your_email'] != $_POST['your_email_comfirm']) {
                if (method_exists($result, 'invalidate')) {
                    $result->invalidate( $tag,"確認用のメールアドレスが一致していません");
                } else {
                    $result['valid'] = false;
                    $result['reason']['your_email_comfirm'] = '確認用のメールアドレスが一致していません';
                }
            }
    }
    return $result;
}

add_filter('wpcf7_validate', 'my_wpcf7_validate', 11, 2);
function my_wpcf7_validate($result,$tags){
  foreach( $tags as $tag ){
    $name = $tag['name'];
    if( $name == 'hide_browser' ){
      $_POST['hide_browser'] = $_SERVER['HTTP_USER_AGENT'];

    }else if( $name == 'hide_datenow' ){
      $_POST['hide_datenow'] = current_time('mysql');
    }
  }
  return $result;
}



/**
 * ページネーション
 * @param  [type] $the_query [description]
 * @return [type]            [description]
 */
function pagenation( $the_query ){

    global $wp_rewrite;

    $paginate_base = get_pagenum_link( 1 );
    if ( strpos( $paginate_base, '?' ) || ! $wp_rewrite->using_permalinks() ) {
      $paginate_format = '';
      $paginate_base = add_query_arg( 'nav', '%#%' );
    } else {
      $paginate_format = ( substr( $paginate_base, -1 ,1 ) == '/' ? '' : '?' ) .
      user_trailingslashit( '?nav=%#%', 'nav' );
      $paginate_base .= '%_%';
    }

    $paginate_links = paginate_links( array(
      'base'      => $paginate_base,
      'format'    => $paginate_format,
      'total'     => $the_query->max_num_pages,
      'mid_size'  => 5,
      'current'   => max( 1, get_query_var('nav') ),
      'prev_text' => '前へ',
      'next_text' => '次へ',
      'type'      => 'array',
    ) );


 if ( $paginate_links ) {
 ?>
    <ul class="c-pagination clearfix">
      <?php foreach ( $paginate_links as $link ) : ?>
      <li class="c-pagination__item"><?php echo $link; ?></li>
      <?php endforeach; ?>
    </ul>

<?php
    }
}
